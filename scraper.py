import requests
from bs4 import BeautifulSoup

# URL = "https://www.ea.com/en-gb/games/fifa/fifa-21/pro-clubs/ps4-xb1-pc/overview?clubId=6663688&platform=ps4"
URL = "https://proclubshead.com/21/club/ps4-6663688/matches-league/"
headers={'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36'}
page = requests.get(URL, headers=headers)

soup = BeautifulSoup(page.content, 'html.parser')
results = soup.findAll("div", {"class": "card p-3"})
players = set()
for result in results:
    name_elem = result.find("a", class_="btn btn--fifa-primary btn-sm")
    name = name_elem["href"].split("/")[-2]
    players.add(name)

print(players)